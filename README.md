# App Title

React Apollo GraphQL App

## Installation
 
Use the package manager [npm](https://www.npmjs.com/) to install dependencies.

```bash
npm install 
```

To start local server and run app

```bash
npm start 
```

## Notes
App is built with GitHub's Api and oAuth as well as React-Apollo-GraphQL stack

## License
[MIT](https://choosealicense.com/licenses/mit/)