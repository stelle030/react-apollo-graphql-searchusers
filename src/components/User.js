import React, { Component } from 'react';
import { Query } from 'react-apollo'
import { gql } from 'apollo-boost';
import Loader from './common/Loader';
import styled from 'styled-components';


const getUser = gql`
  query GetUser($userId: ID!){
    node(id: $userId) {
    ... on User {
        name
        login
        avatarUrl
        url
        repositories {
            totalCount
        }
        followers {
            totalCount
        }
        following {
            totalCount
        }
        starredRepositories {
            totalCount
        }
      }
    }
  }
`

class User extends Component {
    state = {
        id: null,
        results: null
    }

    componentDidMount() {
        let id = this.props.match.params.user_id;
        this.setState({
            id: id
        })
    }

    renderUser = () => {
      let getUserString = {userId:`${this.state.id}`};
        
      let results = <Query query={getUser} variables={getUserString}>
          {({ data, loading, error }) => {
              if(loading) return <Loader />
              if(error) return <p>{error.message}</p>
              else {
                  return(
                      <UserSectionWrapper>
                        <Headline>{`${data.node.login}'s profile`}</Headline>
                        <UserInnerWrapper>
                            <BasicInfo>
                                <UserImage src={data.node.avatarUrl} alt="user_image" />
                                <UserName>{data.node.name}</UserName>
                                <UserNick>{data.node.login}</UserNick>
                            </BasicInfo>
                            <Statistics>
                                <ul>
                                    <li>Repositories: {data.node.repositories.totalCount}</li>
                                    <li>Stars: {data.node.starredRepositories.totalCount}</li>
                                    <li>Followers: {data.node.followers.totalCount}</li>
                                    <li>Following: {data.node.following.totalCount}</li>
                                </ul>
                            </Statistics>
                        </UserInnerWrapper>
                        <GitHubProfileLink>
                            <p>If you would like to visit {`${data.node.login}'s `} full profile on GitHub, click <a rel="noopener noreferrer" href={data.node.url} target='_blank'>here</a></p>
                        </GitHubProfileLink>
                      </UserSectionWrapper>
                  )
              }
          }}
          </Query>

          return results;
    }


    render() {
        return(
            <MainUserWrapper>
                {this.renderUser()}
            </MainUserWrapper>
        )
    }
}

const MainUserWrapper = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    padding-top: 20px;
`

const UserSectionWrapper = styled.div`
    padding: 40px;  
`

const Headline = styled.h2`
    text-align: center;
`

const UserInnerWrapper = styled.div`
    display: flex;
    justify-content: center;
    padding: 60px 0 80px 0;
`

const BasicInfo = styled.div`
    display: flex;
    flex-direction: column;
    text-align: center;
`

const UserImage = styled.img`
    width: 250px
    height: 250px;
`

const UserName = styled.span`
    font-weight: 700;
    font-size: 24px;
`

const UserNick = styled.span`
    font-weight: 200;
`

const Statistics = styled.div`
    display: flex;
`

const GitHubProfileLink = styled.div`
    text-decoration: none;
    display: flex;
    justify-content: center
`


export default User;