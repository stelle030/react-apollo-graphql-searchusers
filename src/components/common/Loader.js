import React from 'react';
import styled, { keyframes } from 'styled-components';

const Loader = () => {
    return(
        <Spinner />
    )
}

const rotate = keyframes`
  from {
      transform: rotate(0deg);
  }
  to {
    transform: rotate(360deg);
  }
`

const Spinner = styled.div`
  border: 4px solid gray;
  border-radius: 50%;
  border-top: 4px solid white;
  width: 30px;
  height: 30px;
  animation: ${rotate} 2s linear infinite;
  self-align: center;
`;

export default Loader;