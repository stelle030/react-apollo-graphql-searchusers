import React from 'react';
import styled from 'styled-components';
import { Link } from 'react-router-dom';
import { FaMapMarkerAlt, FaEnvelope } from 'react-icons/fa';

const SearchResults = (props) => {

    const { data } = props
    
    function renderResults() {
        if (!data.search.nodes.length) {
            return <p>No results found :(</p>
        } else {
            return data.search.nodes.map(({ id, name, login, email, avatarUrl, location, bio }) => (
                    <UserCard key={id}> 
                        <ImageBackLayer>
                            <UserImage src={avatarUrl} /> 
                        </ImageBackLayer>
                        <MainDiv>
                            <TopRow>
                                <UserLink to={`/users/${id}`}>
                                    <UserName>{login}</UserName>
                                </UserLink>
                                <span>{name}</span>
                            </TopRow>
                            <MiddleRow>
                                {bio ? <span>{bio}</span> : null}
                            </MiddleRow>
                            <BottomRow>
                                {location ? <Location><FaMapMarkerAlt size='0.7em' /> {location}</Location> : null}
                                {email ? <span><FaEnvelope size='0.7em' /> {email}</span> : null }
                            </BottomRow>
                        </MainDiv>
                    </UserCard>
            ))
        }

    }
    return renderResults()
}

const UserCard = styled.div`
    display: flex;
    justify-content: flex-start;
    border-top: 1px solid gray;
    padding: 10px 0;
`

const ImageBackLayer = styled.div`
    display: flex;
    justify-content: center;
    align-items: flex-start;
`

const MainDiv = styled.div`
    display: flex;
    flex-direction: column;
`

const UserLink = styled(Link)`
    text-decoration: none;
    color: gray;
`

const UserImage = styled.img`
    width: 48px;
    heigt: 48px;
    margin-right: 20px;
`

const TopRow = styled.div`
    display: flex;
    margin-bottom: 5px;
`
const UserName = styled.span`
    margin-right: 5px;
    color: blue;
`

const MiddleRow = styled.div`
    display: flex;
`

const BottomRow = styled.div`
    display: flex;
`

const Location = styled.span`
    margin-right: 10px;
`


export default SearchResults;