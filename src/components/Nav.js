import React from 'react';
import styled from 'styled-components';
import { FaGithub } from 'react-icons/fa';
import ViewerInfo from './ViewerInfo';
import { Link } from 'react-router-dom';


const Nav = ({ href, isLoggedIn }) => {
    return(
        <Navigation>
            <NavigationWrapper>
                <HomeLink to='/'>
                    <Brand>LOGO</Brand>
                </HomeLink>
                <GitHubLogin>
                    {!isLoggedIn ? <Login href={href}><FaGithub size='1.5em' /> Login with GitHub</Login> : <ViewerInfo />}
                </GitHubLogin>
            </NavigationWrapper>
        </Navigation>
    )
}

const Navigation = styled.div`
  width: 100%;
  background-color: #24292e;
`
const NavigationWrapper = styled.div`
  padding: 0 20px;
  display: flex;
  justify-content: space-between;
`

const HomeLink = styled(Link)`
  text-decoration: none;
`

const Brand = styled.p`
    color: #fff;
    font-size: 20px;
`

const GitHubLogin = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
`

const Login = styled.a`
  text-decoration: none;
  color: #fff;
  border: 1px solid #fff;
  border-radius: 3px;
  padding: 5px 10px;
  display: flex;
  justify-content: space-between;
  align-items: center;
  min-width: 160px;
  transition: 0.3s;

  &:hover {
      background-color: white;
      color: #000;
      border: 1px solid #000;
  }
`

export default Nav;