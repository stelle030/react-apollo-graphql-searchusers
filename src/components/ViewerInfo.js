import React from "react";
import { gql } from "apollo-boost";
import { Query } from "react-apollo";
import styled from 'styled-components';
import Loader from './common/Loader';

const GET_VIEWER_INFO = gql`
  query {
    viewer {
      avatarUrl
      login
    }
  }
`;

class ViewerInfo extends React.Component {
  render() {
    return (
      <Query query={GET_VIEWER_INFO}>
        {({ loading, error, data }) => {
          if (loading) return <Loader />;
          if (error) return <div>Error :(</div>;

          return (
            <ViewerDetails>
                <ViewerName>Signed in as <ViewerSpan>{data.viewer.login}</ViewerSpan></ViewerName>
                <ViewerImage src={data.viewer.avatarUrl}></ViewerImage>
            </ViewerDetails>
          )
        }}
      </Query>
    );
  }
}

const ViewerDetails = styled.div`
  display: flex;
  align-items: center;
`

const ViewerImage = styled.img`
  width: 40px;
  height: 40px;
  border-radius: 30px;
  margin-left: 10px;
`

const ViewerName = styled.p`
  color: #fff;
  margin: 0;
`

const ViewerSpan = styled.span`
  font-weight: 700;
`

export default ViewerInfo;