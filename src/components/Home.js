import React from 'react';
import styled from 'styled-components';
import UserSearch from './UserSearch'

function Home(props) {
    const { isLoggedIn } = props;
    return(
        <Wrapper>
            {!isLoggedIn ? 
            <div>
                <LandingText>Please login to proceed</LandingText>
            </div>
            :
            <>
                <UserSearch />
            </>
            }
        </Wrapper>
    )
}

const Wrapper = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    padding: 100px 0;
    flex-direction: column;
`

const LandingText = styled.h2`
    text-transform: uppercase;
    font-size: 24px;
`

export default Home;