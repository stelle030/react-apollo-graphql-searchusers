import React, { Component } from 'react';
import { FaSearch } from 'react-icons/fa';
import styled from 'styled-components';
import { Query } from 'react-apollo'
import { gql } from 'apollo-boost';
import SearchResults from './SearchResults';
import Loader from './common/Loader';

const searchQuery = gql`
  query userSearch($queryString:String!, $len:Int!){
    search(type:USER, query:$queryString, first:$len){
      nodes{
        ...on User{
          id
          email
          login
          name
          avatarUrl
          location
          bio
          }
        }
     }
  }
`

class UserSearch extends Component {

    state = {
        search: '',
        results: null
    }

    handleChange = (e) => {
        this.setState({
            search: e.target.value
        })
    }

    fecthMore = (fetchMore, data) => {
        let searchString = { queryString: `${this.state.search}`, len: 10 };
        fetchMore({
            variables: { ...searchString, len: data.search.nodes.length + 10 },
            updateQuery: (prev, { fetchMoreResult }) => {
                if (!fetchMoreResult) {
                    console.log(this.state.over)
                    return prev
                }
                return { ...prev, ...fetchMoreResult };
            }
        })
    }

    handleClick = async () => {
        let searchString = { queryString:`${this.state.search}`, len: 10 };
    
        let results = <Query query={searchQuery} variables={searchString}>
            {({ data, loading, error, fetchMore }) => {
                if(loading) return <Loader />
                if(error) return <p>{error.message}</p>
                else {
                    return(
                        <div>
                            <SearchResults data={data} />
                            {data.search.nodes.length > 1 &&
                                <LoadMoreButton onClick={() => this.fecthMore(fetchMore, data)}>Load more</LoadMoreButton>
                            }
                        </div>
                    )
                }
            }}
        </Query>

        this.setState({
            results: results
        })

    }

    render() {

        return(
            <UserSearchDiv>
                <SearchBox>
                    <Headline>
                        <Title>Search for users on GitHub</Title>
                    </Headline>
                    <SearchText>
                        <Button onClick={this.handleClick} disabled={!this.state.search}>
                            <FaSearch size='1.5em'/>
                        </Button>
                        <Input 
                            onKeyUp={(e) => {
                                if (e.keyCode === 13) {
                                    this.handleClick()
                                    return false
                                    }
                            }}      
                            type='text' 
                            placeholder='Search...' 
                            onChange={this.handleChange} 
                            value={this.state.search} 
                        />
                    </SearchText>
                    <SearchResultsContainer>
                        {this.state.results && this.state.results}
                    </SearchResultsContainer>
                </SearchBox>
            </UserSearchDiv>
        )
    }
}

const LoadMoreButton = styled.button`
    padding: 10px 70px;
    background-color: rgb(0, 123, 250);
    border-radius: 10px;
    border: none;
    text-transform: uppercase;
    color: white;
    cursor: pointer;
    outline: none;
`

const UserSearchDiv = styled.div`
    display: flex;
    flex-direction: column;
    background-color: #f1f1f1;
    padding: 0 300px;
    box-shadow: -1px 2px 7px 3px rgba(0,0,0,0.31)
`

const SearchBox = styled.div`
    display: flex;
    flex-direction: column;
`
const Headline = styled.div`
    margin-top: 20px;
    margin-bottom: 40px;
`

const Title = styled.h1`
    font-size: 42px;
    text-transform: uppercase;
    letter-spacing: 1px;
`

const SearchText = styled.div`
    border: 1px solid rgba(0, 0, 0, 0.1);;
    border-radius: 10px;
    display: flex;
    background-color: transparent;
    border-radius: 30px;
    padding: 10px;
    margin-bottom: 100px;
`

const Input = styled.input`
    height: 30px;
    font-size: 16px;
    padding: 5px;
    border: none;
    outline: none;
    background-color: transparent;
    width: 100%;
`

const Button = styled.button`
    background-color: transparent;
    border: none;
    outline: none;
`

const SearchResultsContainer = styled.div`
    display: flex;
    justify-content: center;
    padding: 50px 0;
`



export default UserSearch;