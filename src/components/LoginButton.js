import React from 'react';
import styled from 'styled-components';
import { FaGithub } from 'react-icons/fa';

const LoginButton = ({ href }) => {
    return(
        <GitHubLogin>
          <Login href={href}><FaGithub size='1.5em' /> Login with GitHub</Login>
        </GitHubLogin>
    )
}

const GitHubLogin = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
`

const Login = styled.a`
  text-decoration: none;
  color: #000;
  border: 1px solid #000;
  border-radius: 3px;
  padding: 5px 10px;
  display: flex;
  justify-content: space-between;
  align-items: center;
  min-width: 160px;
  transition: 0.3s;

  &:hover {
    background-color: #cecccc;
    color: #000000;
    border: 1px solid #000;
  }
`

export default LoginButton;