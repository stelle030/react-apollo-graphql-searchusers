import React, { Component } from 'react';
import './App.css';
import Nav from './components/Nav';
import fetch from 'unfetch';
import ApolloClient from 'apollo-boost';
import { ApolloProvider } from '@apollo/react-hooks';
import { BrowserRouter, Route } from 'react-router-dom';
import Home from './components/Home';
import User from './components/User';

const CLIENT_ID = "4160de447588878a597d";
const REDIRECT_URI = "http://localhost:3000/";

const client = new ApolloClient({
  uri: 'https://api.github.com/graphql',
  request: operation => {
    const token = localStorage.getItem('token');
    if (token) {
      operation.setContext({
        headers: {
          authorization: `Bearer ${token}`
        }
      });
    }
  }
});

class App extends Component {
  state = {
    isLoggedIn: false,
    token: null
  };

  componentDidMount() {
    // Check to see if token exists in localstorage and set isLoggedIn to true if it does
    const storedToken = localStorage.getItem('token');
    if (storedToken) {
      this.setState({
        isLoggedIn: true,
        token: storedToken
      });
      return;
    } 
    // RegEx - Get code that GitHub returns
    const code = 
    window.location.href.match(/\?code=(.*)/) && 
    window.location.href.match(/\?code=(.*)/)[1];
    if (code) {
      // Exchange code for token
      fetch(`https://dfds-assignment-gatekeeper-app.herokuapp.com/authenticate/${code}`)
        .then(response => response.json())
         .then(({ token }) => {
           localStorage.setItem('token', token);
           this.setState({
             isLoggedIn: true,
             token
           })
           console.log(token);
         })
    }
  }
  
  render() {
    return (
      <ApolloProvider client={client}>
        <BrowserRouter>
          <div className="App">
            <Nav isLoggedIn={this.state.isLoggedIn} href={`https://github.com/login/oauth/authorize?client_id=${CLIENT_ID}&scope=user&redirect_uri=${REDIRECT_URI}`} />
            <Route exact path='/' render={()=> <Home isLoggedIn={this.state.isLoggedIn} href={`https://github.com/login/oauth/authorize?client_id=${CLIENT_ID}&scope=user&redirect_uri=${REDIRECT_URI}`} />} />
            <Route path='/users/:user_id' component={User} />
          </div>
        </BrowserRouter>
      </ApolloProvider>
      
    );
  }
}

export default App;
